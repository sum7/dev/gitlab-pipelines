# Default Pipelines for GitLab

## Container-Images

```yaml
---
include:
  - project: sum7/dev/gitlab-pipelines
    ref: main
    file:
      - container.yml
```

Required:
 - needs `Containerfile`

Features are:
 - Build Container on every branch and tag
 - Publish on Gitlabs container registry
   - Tags of Container are updated based on Semantic Versioning

